public class SavingsAccount extends Account {
    public SavingsAccount(String s, double d) {
        super (s,d);
    }

    @Override
    public void addInterest() {
        this.setBalance(getBalance() * 1.4);
    }

    @Override
    public String getDetails() {
        return null;
    }
}
