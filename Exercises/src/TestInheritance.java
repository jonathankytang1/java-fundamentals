public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = {
                new Account("A", 2),
                new Account("B", 4),
                new Account("C", 6),
        };

        for (int i = 0; i < accounts.length; ++i) {
            accounts[i].addInterest();
            System.out.println("The name is: " + accounts[i].getName());
            System.out.println("The balance is:  " + accounts[i].getBalance());
            System.out.println("\n");
        }

    }
}
