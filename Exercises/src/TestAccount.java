public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account();

        myAccount.setName("JT");
        myAccount.setBalance(5);

        System.out.println("Name: " + myAccount.getName());
        System.out.println("Balance: " + myAccount.getBalance());

        myAccount.addInterest();
        System.out.println("Balance after interest: " + myAccount.getBalance());

        Account[] arrayOfAccounts;
        arrayOfAccounts = new Account[5];

        double[] amounts = {5, 10, 15, 20, 25};
        String[] names = {"AT", "BT", "CT", "DT", "ET"};

        for (int i = 0; i < arrayOfAccounts.length; ++i) {
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            System.out.println("Name: " + arrayOfAccounts[i].getName());
            System.out.println("Balance: " + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("New balance: " + arrayOfAccounts[i].getBalance() + "\n");
        }
    }
}
